(use-modules (guix gexp)
	     (guix packages)
	     ((guix licenses) #:prefix license:)
	     (guix download)
	     (guix build-system gnu)
	     (gnu packages)
	     (gnu packages guile)
	     (gnu packages guile-xyz)
	     (srfi srfi-1))

(define (include-file? filename stat)
  (not (member filename '(".git" "guix.scm"))))

(package
  (name "scribe")
  (version "0.1")
  (source (local-file (dirname (current-filename))
                      #:recursive? #t
                      #:select? include-file?))
  (build-system gnu-build-system)
  (arguments
   `(#:phases
     (modify-phases %standard-phases
       (replace 'bootstrap
         (lambda _
           (invoke "autoreconf" "-vif"))))))
  (inputs (list guile-3.0))
  (propagated-inputs
    (list guile-goblins))
  (synopsis
    "IRC Bot to log channels & record meetings")
  (description
    "Scribe is an IRC bot which will produce plain text logs of IRC channels as well as allow for meetings to be scrbed. The meeting functionality allows for setting a scribe who's messages will be recorded as the meeting minutes.")
  (home-page "")
  (license license:gpl3+))

