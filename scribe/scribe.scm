(use-modules (goblins)
	     (goblins vat)
	     (goblins actor-lib methods)
	     (goblins actor-lib cell)
	     (goblins actor-lib ward)
	     (goblins persistence-store syrup)
	     (fibers)
	     (fibers conditions)
	     (scribe irc)
	     (scribe logging)
	     (scribe utils)
	     (ice-9 match))

(define-actor (^irc-network* bcom connection
			     message-dispatcher
			     nickname realname
			     channels
			     #:optional init-setup?)
  #:restore
  (lambda (version connection message-dispatcher nickname realname channels _setup?)
    ;; NOTE: We ignore setup as the network is never "setup" on restore.
    (spawn ^irc-network* connection message-dispatcher nickname realname channels))
	      
  ;; Always send the initial messages to connect to the network
  (define setup?
    (if init-setup?
	#t
	(begin
	  (<-np connection 'send-message (format #f "NICK ~a" nickname))
	  (<-np connection 'send-message (format #f "USER ~a * *: ~a" nickname realname))
	  (for-each
	   (lambda (channel)
	     (<-np connection 'send-message (format #f "JOIN ~a" channel)))
	   channels)
	  #t)))
  
  (methods
   [(register-handler match? dispatch-to)
    (<-np message-dispatcher match? dispatch-to)]
   [(register-handler-for-channels make-match? make-dispatch-to)
    (for-each
     (lambda (channel)
       (<-np message-dispatcher
	     (make-match? channel)
	     (make-dispatch-to channel)))
     channels)]
   [(send-message to msg)
    (<-np connection 'send-message
	  (format #f "PRIVMSG ~a :~a" to msg))]
   [(send-raw msg)
    (<-np connection 'send-message msg)]
   [(quit msg)
    (<-np connection 'send-message (format #f "QUIT :~a" msg))]
   [(set-nick new-nickname)
    (<-np connection 'send-message (format #f "NICK ~a" new-nickname))
    (bcom (^irc-network* bcom connection message-dispatcher
			 new-nickname realname channels setup?))]
   [(join channel)
    (<-np connection 'send-message (format #f "JOIN ~a" channel))
    (bcom (^irc-network* bcom connection message-dispatcher
			 nickname realname
			 (cons channel channels)
			 setup?))]))

(define (^irc-network bcom address port nickname realname channels)
  (define-values (connection message-dispatcher)
    (spawn-socket-dispatcher-pair address port))
  (spawn ^irc-network*
	 connection message-dispatcher
	 nickname realname
	 channels))

(define-actor (^irc-bot _bcom network)
  "IRC bot on a specific network"

  ;; Setup ping responding
  (define (^ping-pong _bcom)
    (lambda (msg)
      (<-np network 'send-raw
	    (format #f "PONG :~a" (irc-message-content msg)))))
  (<-np network 'register-handler
	(lambda (msg)
	  (eq? (irc-message-type msg) 'PING))
	(spawn ^ping-pong))

  ;; Register a logger for each channel
  (<-np network 'register-handler-for-channels
	(lambda (channel)
	  (lambda (message)
	    (equal? (irc-message-channel message) channel)))
	(lambda (channel)
	  (spawn ^logger channel "/tmp/scribe-logs/")))

  ;; Allow for joining new channels
  (define (^channel-joiner _bcom)
    (lambda (message)
      (match (string-split (irc-message-content message) #\space)
	[("!join" channel)
	 (<-np network 'join channel)
	 (<-np network 'send-message
	       (irc-message-channel message)
	       (format #f "Joined ~a!" channel))]
	[other
	 (<-np network 'send-message
	       (irc-message-channel message)
	       "Unkown join command, syntax: !join <channel>")])))
  (<-np network 'register-handler
	(lambda (message)
	  (and (equal? (irc-message-type message) 'PRIVMSG)
	       (string? (irc-message-content message))
	       (string-prefix? "!join" (irc-message-content message))))
	(spawn ^channel-joiner))

  (define (^nick-changer _bcom)
    (lambda (message)
      (match (string-split (irc-message-content message) #\space)
	[("!nick" new-nickname)
	 (<-np network 'set-nick new-nickname)]
	[otherwise
	 (<-np network 'send-message
	       (irc-message-channel message)
	       "Unknown nick command, syntax: !nick <new-nickname>")])))
  (<-np network 'register-handler
	(lambda (message)
	  (and (equal? (irc-message-type message) 'PRIVMSG)
	       (string? (irc-message-content message))
	       (string-prefix? "!nick" (irc-message-content message))))
	(spawn ^nick-changer))
	    
  
  (lambda () 'noop))

(define bot-env
  (make-persistence-env
   `((((scribe) ^irc-bot) ,^irc-bot)
     (((scribe) ^irc-network) ,^irc-network*)
     (((scribe irc) ^message-dispatcher) ,^message-dispatcher)
     (((scribe irc) ^socket) ,^socket))
   #:extends (list ward-env cell-env)))

(define-values (irc-vat irc-bot)
  (spawn-persistent-vat
   bot-env
   (lambda ()
     (define libera-network
       (spawn ^irc-network "irc.libera.chat" 6667
	      "scribebot" "Scribe Bot" '("#tsyesika-bot-test")))
     (spawn ^irc-bot libera-network))
   (make-syrup-store "scribe-bot.state")))

(define forever (make-condition))
(wait forever)
  
