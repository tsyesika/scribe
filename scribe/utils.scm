(define-module (scribe utils)
  #:use-module (srfi srfi-19)
  #:export (date=?
	    current-date-utc))

(define (date=? d1 d2)
  "Checks if two dates are equal"
  (and (date? d1) (date? d2)
       (eq? (date-year d1) (date-year d2))
       (eq? (date-month d1) (date-month d2))
       (eq? (date-day d1) (date-day d2))))

(define (current-date-utc)
  "Gets the current date in UTC"
  (current-date 0))
	
