(define-module (scribe irc)
  #:use-module (srfi srfi-9)
  #:use-module (ice-9 match)
  #:use-module (ice-9 atomic)
  #:use-module (ice-9 rdelim)
  #:use-module (ice-9 textual-ports)
  #:use-module (goblins)
  #:use-module (goblins vat)
  #:use-module (goblins actor-lib cell)
  #:use-module (goblins actor-lib io)
  #:use-module (goblins actor-lib methods)
  #:use-module (goblins actor-lib ward)
  #:export (<irc-network>
	    make-irc-network
	    irc-network?
	    irc-network-address
	    irc-network-port
	    irc-network-password
	    
	    <irc-user>
	    make-irc-user
	    irc-user?
	    irc-user-nickname
	    irc-user-realname

	    <irc-message>
	    irc-message?
	    irc-message-user
	    irc-message-type
	    irc-message-channel
	    irc-message-content

	    ^message-dispatcher
	    ^socket
	    spawn-socket-dispatcher-pair))

(define-record-type <irc-network>
  (make-irc-network address port password)
  irc-network?
  (address irc-network-address)
  (port irc-network-port)
  (password irc-network-password))
  
(define-record-type <irc-user>
  (make-irc-user nickname realname)
  irc-user?
  (nickname irc-user-nickname)
  (realname irc-user-realname))

(define-record-type <irc-message>
  (make-irc-message user type channel content)
  irc-message?
  (user irc-message-user)
  (type irc-message-type)
  (channel irc-message-channel)
  (content irc-message-content))

(define (string->irc-user str)
  "Converts a user to a <irc-user> user"
  ;; It's formatted as ":nickname!~realname@hostname"
  (let* ((!-pos (string-contains str "!"))
	 (@-pos (string-contains str "@")))
    (if (and !-pos @-pos)
	(let* ((nickname (substring str 1 !-pos))
	       (nickname-len (string-length nickname))
	       ;; +3 because we need to skip the "!~" and leading ":"
	       (realname (substring str (+ nickname-len 3) @-pos)))
	  (make-irc-user nickname realname))
	#f)))

(define (string->irc-message str)
  "Converts a message to a <irc-message> message"
  (define (channel? maybe-channel)
    (string-prefix? "#" maybe-channel))
  (define (content? maybe-content)
    (string-prefix? ":" maybe-content))
  (define (content->string lst)
    (let ((content (string-join lst " ")))
      (if (string-prefix? ":" content)
	  (substring content 1)
	  content)))
  (match (string-split (string-trim-right str #\return) #\space)
    ;; For example PING :blah.tld
    ((type (? content? content) rest ...)
     (make-irc-message #f
		       (string->symbol type)
		       #f
		       (content->string (cons content rest))))
    ;; For example :foo@~bar!blah.tld PRIVMSG #channel :hello
    ((user type (? channel? channel) content ...)
     (make-irc-message (string->irc-user user)
		       (string->symbol type)
		       channel
		       (content->string content)))
    ;; For example: :foo@~bar!blah.tld QUIT :goodbye
    ((user type content ...)
     (make-irc-message (string->irc-user user)
		       (string->symbol type)
		       #f
		       (content->string content)))
    (msg (error "Unknown message format: " msg))))

(define (irc-socket-create)
  (let* ((s (socket PF_INET SOCK_STREAM 0))
         (flags (fcntl s F_GETFL)))
    s))

(define* (irc-socket-setup sock hostname #:optional (inet-port 6667))
  (let ((ip-address (inet-ntop AF_INET (car (hostent:addr-list (gethost hostname))))))
    (connect sock AF_INET
             (inet-pton AF_INET ip-address)
             inet-port)))

(define-actor (^socket bcom address port dispatcher dispatch-incanter
		       #:optional disconnected?)
  "Connects to a socket and reads and writes messages from said socket"
  (define socket
    (spawn ^read-write-io (irc-socket-create)
	   #:init
	   (lambda (sock)
	     (irc-socket-setup sock address port))
	   #:cleanup
	   (lambda (sock)
	     (close sock))))

  ;; Kick off the get message "loop"
  (get-message)
  
  (define (write-message message)
    (<-np socket 'write
	  (lambda (port)
	    (put-string port (format #f "~a\r\n" message))
	    (force-output port))))

  (define (get-message)
    (on (<- socket 'read
	    (lambda (port)
	      (read-line port 'trim)))
	(lambda (message)
	  (<-np-extern dispatch-incanter dispatcher
		       (string->irc-message message)))
	#:finally get-message))

  (define (disconnected-beh)
    ;; TODO: do we want a connect method instead of just error all the time?
    (error "Socket disconnected"))

  (define active-beh
    (methods
     [send-message write-message]
     [(disconnect)
      (<-np socket 'halt)
      (bcom (^socket bcom address port dispatcher dispatch-incanter #t))]))

  (if disconnected?
      disconnected-beh
      active-beh))

(define-actor (^message-dispatcher _bcom socket warden)
  ;; This by design is not remembered.
  (define dispatchers (spawn ^cell '()))

  (define (register-beh message-match? target)
    "Recieve messages which match @{message-match?} and dispatching them to @{target}"
    ($ dispatchers (cons (list message-match? target) ($ dispatchers)))
    'registered)
  (define (sock-beh message)
    "Recieve a message to dispatch"
    (let* ((dispatchers ($ dispatchers))
	   (matching-disaptchers
	    (filter
	     (match-lambda
	       [(matcher _target) (matcher message)])
	     dispatchers)))
      (for-each
       (match-lambda
	 ((matcher target) (<-np target message)))
       matching-disaptchers))
    'dispatched)
  (ward warden sock-beh
	#:extends register-beh))

(define (spawn-socket-dispatcher-pair address port)
  (define-values (warden incanter)
    (spawn-warding-pair))
  (define-values (socket-vow socket-resolver)
    (spawn-promise-values))
  (define dispatcher
    (spawn ^message-dispatcher socket-vow warden))
  (define socket
    (spawn ^socket address port dispatcher incanter))
  ($ socket-resolver 'fulfill socket)
  (values socket dispatcher))
	  
