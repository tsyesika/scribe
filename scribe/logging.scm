(define-module (scribe logging)
  #:use-module (scribe utils)
  #:use-module (scribe irc)
  #:use-module (goblins)
  #:use-module (goblins vat)
  #:use-module (goblins actor-lib cell)
  #:use-module (goblins actor-lib io)
  #:use-module (srfi srfi-19)
  #:use-module (ice-9 textual-ports)
  #:export (^logger))

(define (^rotating-file-access _bcom base-path name)
  (define date-today
    (spawn ^cell (current-date-utc)))
  (define io (spawn ^cell))
  (define (create-io)
    (when ($ io)
      (<-np ($ io) 'halt))
    ($ io
       (spawn ^io
	      (open-output-file
	       (format #f "~a/~a-~a.txt" base-path name
		       (date->string ($ date-today) "~Y-~m-~d")))
	      #:cleanup
	      (lambda (port)
		(close-port port)))))
  (define (need-new-io?)
    (not (and ($ io)
	      (date=? ($ date-today) (current-date-utc)))))
  (lambda args
    ;; Proxy args
    (when (need-new-io?)
      (create-io))
    (apply <-np ($ io) args)))

(define (^logger _bcom name output-dir)
  "Logs all messages received into output-dir"
  (define rotating-file-access
    (spawn ^rotating-file-access output-dir name))

  (define (format-privmsg message)
    (format #f "<~a> ~a"
	    (irc-user-nickname (irc-message-user message))
	    (irc-message-content message)))

  (define (format-status message)
    (format #f "* ~a ~as"
	    (irc-user-nickname (irc-message-user message))
	    (string-downcase (symbol->string (irc-message-type message)))))
  
  (lambda (message)
    (define date-now
      (current-date-utc))
    
    (define format-message
      (cond ((eq? (irc-message-type message) 'PRIVMSG) format-privmsg)
	    ((eq? (irc-message-type message) 'QUIT) format-status)
	    ((eq? (irc-message-type message) 'JOIN) format-status)))
    
    (define formatted-message
      (format #f "~a ~a\n"
	      (date->string date-now "[~H:~M]")
	      (format-message message)))

    (<-np rotating-file-access
	  (lambda (port)
	    (put-string port formatted-message)
	    (force-output port)))))
